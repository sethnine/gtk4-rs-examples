// mod integer_object;

use std::cell::RefCell;
use std::rc::Rc;

use gtk::glib;
use gtk::{prelude::*, ListItem};
use gtk::{
    Application, ApplicationWindow, CustomFilter, FilterListModel, Label, ListView, PolicyType,
    ScrolledWindow, SignalListItemFactory, SingleSelection, StringList, StringObject,
};
// use integer_object::IntegerObject;

const APP_ID: &str = "org.gtk_rs.ListWidgets2";

fn main() -> glib::ExitCode {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run()
}

fn build_ui(app: &Application) {
    let a = Rc::new(RefCell::new("".to_string()));
    let a2 = a.clone();
    // Create model
    let model: StringList = (0..=100_000).map(|number| number.to_string()).collect();

    let factory = SignalListItemFactory::new();
    factory.connect_setup(move |_, list_item| {
        let label = Label::new(None);
        let list_item = list_item
            .downcast_ref::<ListItem>()
            .expect("Needs to be ListItem");
        list_item.set_child(Some(&label));

        // list_item
        //     .property_expression("item")
        //     .chain_property::<IntegerObject>("number")
        //     .bind(&label, "label", gtk::Widget::NONE);
        list_item
            .property_expression("item")
            .chain_property::<StringObject>("string")
            .bind(&label, "label", gtk::Widget::NONE);
    });

    let filter = CustomFilter::new(move |obj| {
        // let integer_object = obj
        //     .downcast_ref::<IntegerObject>()
        //     .expect("The object need to be of the type integer object.");
        // integer_object.number() % 2 == 0
        let string_object = obj
            .downcast_ref::<StringObject>()
            .expect("this needs to be a string object dude");
        let filter_str = a.borrow();
        let filter_str = filter_str.as_str();
        Into::<String>::into(string_object).contains(filter_str)
    });
    let filter_model = FilterListModel::new(Some(model), Some(filter.clone()));
    let selection_model = SingleSelection::new(Some(filter_model));
    let list_view = ListView::new(Some(selection_model), Some(factory));

    list_view.connect_activate(move |list_view, position| {
        let model = list_view.model().expect("The model needs to exist");
        // let integer_object = model
        //     .item(position)
        //     .and_downcast::<IntegerObject>()
        //     .expect("the item has to be an integer object");
        let string_object = model
            .item(position)
            .and_downcast::<StringObject>()
            .expect("this should be a string object");
        let string = string_object.string();
        // println!("user clicked {}", string);
        sub_window(string.to_string());
        // integer_object.inc_number();
        // &filter.changed(gtk::FilterChange::Different);
        // sorter.changed(gtk::SorterChange::Different);
    });

    let input = gtk::Entry::builder().build();
    input.connect_changed(move |entry: &gtk::Entry| {
        let b = entry.text();
        let b = b.as_str().to_string();
        a2.replace(b);
        filter.changed(gtk::FilterChange::Different);
    });

    input.connect_activate(move |_| {
        // enter was presed on input box
        println!("input was activated");

        // TODO: Somehow get the first element in the list_view and
        // send it to sub_window as a string

        // let b = entry.text();
        // let b = b.as_str().to_string();
        // a2.replace(b);
        // &filter.changed(gtk::FilterChange::Different);
    });

    // ANCHOR: scrolled_window
    let scrolled_window = ScrolledWindow::builder()
        .hscrollbar_policy(PolicyType::Never) // Disable horizontal scrolling
        .min_content_width(360)
        .vexpand(true)
        .child(&list_view)
        .build();
    let container = gtk::Box::builder()
        .orientation(gtk::Orientation::Vertical)
        .spacing(0)
        .build();

    container.append(&input);
    container.append(&scrolled_window);

    // Create a window
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .default_width(600)
        .default_height(300)
        .child(&container)
        .build();

    // Present window
    window.present();
    // ANCHOR_END: scrolled_window
}

fn sub_window(string: String) {
    let vector = vec![string.clone(), string.chars().rev().collect()];

    let list_box = gtk::ListBox::new();
    for str in vector.iter() {
        let label = Label::new(Some(&str));
        list_box.append(&label);
    }

    let scrolled_window = ScrolledWindow::builder()
        .hscrollbar_policy(PolicyType::Never) // Disable horizontal scrolling
        .min_content_width(360)
        .vexpand(true)
        .child(&list_box)
        .build();
    let window = gtk::Window::builder()
        .title("test")
        .default_width(600)
        .default_height(300)
        .child(&scrolled_window)
        .build();

    window.present();

    list_box.connect_row_activated(|_, row| {
        let row_obj = row
            .child()
            .and_downcast_ref::<Label>()
            .unwrap()
            .text()
            .to_string();
        sub_window(row_obj)
    });

    // List_box.connect_activate(move |list_view, position| {
    //     let model = list_view.model().expect("The model needs to exist");
    //     // let integer_object = model
    //     //     .item(position)
    //     //     .and_downcast::<IntegerObject>()
    //     //     .expect("the item has to be an integer object");
    //     let string_object = model
    //         .item(position)
    //         .and_downcast::<StringObject>()
    //         .expect("this should be a string object");
    //     let string = string_object.string();
    //     // println!("user clicked {}", string);
    //     bob(string.to_string());
    //     // integer_object.inc_number();
    //     // &filter.changed(gtk::FilterChange::Different);
    //     // sorter.changed(gtk::SorterChange::Different);
    // });
}
